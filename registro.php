<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="src/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <title>Papeleria</title>
  </head>
  <body>
      <!--contenedor de todo el login-->
<div class="container-all">
<!--contenedor izquierdo-->
<div class="container-left">
    <div class="front"></div>
<!--Texto contenedor izquierdo-->
    <h1 class="title-descripcion">Papeleria</h1>
<p class="text-container">Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
    Esse quas aliquam nam inventore natus quaerat velit officia cum sunt. Quas debitis rerum pariatur sit maxime sapiente modi, adipisci voluptas voluptatum.</p>
</div>
    
<!--contenedor parte derecha login-->
    <div class="container">
        <img src="#" alt="" class="logo">
    <h1 class="title">Registrate</h1>
    <form action="validar-registro.php" method="POST">
        <!--Nombre-->
        <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="NombreHelp" placeholder="Ingrese su nombre" name="nombre" required>
            </div>
        <!--Apellido -->
        <div class="form-group">
              <label for="exampleInputEmail1">Apellido</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="ApellidoHelp" placeholder="Ingrese su apellido" name="apellido" required>
            </div>
        <!--Email-->
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese su email" name="correo" required>
              <small id="emailHelp" class="form-text text-muted">Ingresa un correo electrónico existente.</small>
            </div>
            <!--Usuario -->
            <div class="form-group">
              <label for="exampleInputEmail1">Usuario</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="UsuarioHelp" placeholder="Ingrese su nombre de usuario" name="usuario" required>
            </div>
            <!--Contraseña -->
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Ingrese su contraseña" name="clave" required>
            </div>
             <!--Confrimar Contraseña -->
             <div class="form-group">
              <label for="exampleInputPassword1">Confirmar Contraseña</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirme su contraseña" name="clave2" required>
            </div>
            <button type="submit" class="btn btn-primary">Iniciar</button>
          </form>
          <div class="icons">
                <a  id="social-icons" href="https://www.instagram.com/pcmastercancun/?hl=es-la"><br><i style="margin-top:30px;" class="fab fa-instagram"></i></a>
                <a  id="social-icons" href="https://www.facebook.com/PcMasterCancun/?ref=bookmarks"><i class="fab fa-facebook"></i></a>
                <a  id="social-icons" href="#"><i class="fab fa-whatsapp"></i></a>
          </div>
          <span class="text-down">¿Ya tienes cuenta? <a href="login.php">Inicia sesión</a> </span>
</div>

</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
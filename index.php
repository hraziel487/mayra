<?php include 'conexion.php';?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="src/css/index.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
    <!--SweetAlert2-->
    <script src="dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="dist/sweetalert2.min.css">
      <!--Javascript-->
   

    <title>MainLine</title>
  </head>
  <body>

  <nav style="background:white" class="navbar navbar-expand-md  navbar-dark">
  <a class="navbar-brand" href="#">MainLine</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span  class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto ">
      <li class="nav-item">
        <a class="nav-link" href="#">INICIO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">SERVICIOS</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="#">OFERTAS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">CONTACTO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">INICIA SESIÓN</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="registro.php">REGISTRATE</a>
      </li>
      <!--Iconos-->  
      <li class="nav-item">
        <a class="nav-link" href="#"><i id="icons" class="fab fa-facebook-f"></i></a>
      </li>    
      <li class="nav-item">
        <a class="nav-link" href="#"><i id="icons" class="fab fa-instagram"></i></a>
      </li>    
      <li class="nav-item">
        <a class="nav-link" href="#"><i id="icons" class="fab fa-twitter"></i></a>
      </li>    
    </ul>
  </div>  
</nav>
<div id="carouselExampleControls" style="background:gray"class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img id="image-box"  class="d-block w-100" src="imagenes/img1.jpg" alt="First slide">
        <div class="container">
            <h1 class="title-slide" id="list-item-1">BIENVENIDOS A MAINLINE</h1>
        <p class="subtitle-slide">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque repudiandae aspernatur accusantium ullam voluptas unde, autem ipsa incidunt, est molestias molestiae aliquid sunt, eos nemo id saepe laborum consectetur officiis.</p>
        <button  id="boton1" type="button" class="btn btn-secondary ">Secondary</button>
        </div>
      </div>
      <div class="carousel-item">
        <img  id="image-box" class="d-block w-100" src="imagenes/img2.jpg" alt="Second slide">
        <h1 class="title-slide2" id="list-item-1">Conocenos</h1>
        <p class="subtitle-slide2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque repudiandae aspernatur accusantium ullam voluptas unde, autem ipsa incidunt, est molestias molestiae aliquid sunt, eos nemo id saepe laborum consectetur officiis.</p>
        <button  id="boton2" type="button" class="btn btn-secondary ">Secondary</button>
        <button  id="boton4" type="button" class="btn btn-secondary ">Secondary</button>
      </div>
      <div class="carousel-item">
        <img  id="image-box" class="d-block w-100" src="imagenes/img3.jpg" alt="Third slide">
        <h1 class="title-slide3" id="list-item-1">Acerca de</h1>
        <p class="subtitle-slide3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque repudiandae aspernatur accusantium ullam voluptas unde, autem ipsa incidunt, est molestias molestiae aliquid sunt, eos nemo id saepe laborum consectetur officiis.</p>
        <button  id="boton3" type="button" class="btn btn-secondary ">Secondary</button>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      
      
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
     
      
    </a>
  </div>
  <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example">
    <div class="container-1-all">
    <div class="container-left">
      <img class="icon" src="imagenes/tijeras.svg" alt="Icono">
      <h4 id="list-item-1">Titulo 1</h4>
    <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
  </div>
  <div class="container-center">
      <img class="icon" src="imagenes/conjunto-cuadrado.svg" alt="Icono">
    <h4 id="list-item-1">Titulo 2</h4>
    <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
  </div>
  <div class="container-rigth">
      <img class="icon" src="imagenes/impresora.svg" alt="Icono">
    <h4 id="list-item-1">Titulo 3</h4>
    <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
  </div>
</div>
  <div class="container-2">

    <div  class="container">
<div class="row">
          <?php
      
            $query = "SELECT * FROM productos ORDER BY id ASC LIMIT 4";
            $result = mysqli_query($conexion,$query);
            if(mysqli_num_rows($result) > 0) {

                while ($row = mysqli_fetch_array($result)) {

                    ?>
                    <div class="col-md-3">
<br>
                        <form  id="productos" method="post" action="Cart.php?action=add&id=<?php echo $row["id"]; ?>">

                            <div class="product" style="background:transparent; " >
                                <img src="imagenes/<?php echo $row["img"]; ?>"  class="img-responsive" style="margin:auto;">
                                <h5 class="text"><?php echo $row["name"]; ?></h5>

                                <h5 class="text"><?php echo $row["precio"]; ?></h5>
                                <!---Cantidad en stock -->
						
                              <?php
					          
					         
					
					          ?>
                               <!---Cantidad a elegir -->
                               
                                <input type="hidden" name="name" value="<?php echo $row["name"]; ?>">
                                <input type="hidden" name="precio" value="<?php echo $row["precio"]; ?>">
                                
					
                           
                            </div>
                        </form>
                    </div>
                    
                    <?php
					
                }
            }
        ?>
  </div>
</div>
</div>

  <div class="container-3">
      <div class="container-left2">
          <img class="icon" src="imagenes/tijeras.svg" alt="Icono">
          <h4 id="list-item-1">Titulo 1</h4>
        <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
      </div>

    <div class="container-center2">
        <img class="icon" src="imagenes/conjunto-cuadrado.svg" alt="Icono">
      <h4 id="list-item-1">Titulo 2</h4>
      <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
    </div>
    <div class="container-rigth2">
        <img class="icon" src="imagenes/impresora.svg" alt="Icono">
      <h4 id="list-item-1">Titulo 3</h4>
      <p>Do aute ipsum ipsum ullamco cillum consectetur ut et aute consecte</p>
    </div>  
  </div>

  <div class="container-4">  
    <div class="container">  
    <form class="Formulario">
     <center> <h2 class="titulo-contacto">Contacto</h2></center>
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Asunto</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div>
          <label for="exampleInputEmail1">Telefono</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         
        <div class="form-group">
        <label for="comment">Comment:</label>
        <textarea  class="form-control" rows="5" id="comment"></textarea>
      </div>
    
        <button type="submit" id="Enviar" class="boton">Enviar</button>
      </div>
      </form>
  <video  autoplay loop muted  width="100%" >
    <source src="video.mov" type="video/mp4">
    <source src="video.mov" type="video/webm">
    <source src="video.mov" type="video/ogg">
      
    </video>
    
      </div>
    </div>
  </div>
  <footer class="footer-distributed">
 
		<div class="footer-left">
 
		<h3>MainLine</h3>
 
		<p class="footer-links">
		<a href="#">Inicio</a>
	·
		<a href="#">Servicios</a>
	·
		<a href="#">Ofertas</a>
	·
		<a href="#">Contacto</a>
	·
		<a href="#">Inicio de sesion</a>
	·
		<a href="#">Registro</a>
		</p>
 
		<p class="footer-company-name">MainLine&copy; 2019</p>
		</div>
 
		<div class="footer-center">
 
		<div>
		<i class="fa fa-map-marker"></i>
		<p><span>Calle Pez vela 24</span> Cancun, Quintana Roo</p>
		</div>
 
		<div>
		<i class="fa fa-phone"></i>
		<p> 998 260 3836</p>
		</div>
 
		<div>
		<i class="fa fa-envelope"></i>
		<p><a href="#">contacto@MainLine.com</a></p>
		</div>
 
		</div>
 
		<div class="footer-right">
 
		<p class="footer-company-about">
		<span>Acerca de</span>
Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia nam reprehenderit doloribus quisquam asperiores. Atque non nobis obcaecati quae odio soluta aspernatur nisi, et eaque nam excepturi totam illum ratione!
		</p>
 
		<div class="footer-icons">
 
		<a href="#"><i class="fa fa-facebook"></i></a>
		<a href="#"><i class="fa fa-twitter"></i></a>
		<a href="#"><i class="fa fa-linkedin"></i></a>
		<a href="#"><i class="fa fa-github"></i></a>
 
		</div>
 
		</div>
 
		</footer>
      
  </body>
</html>
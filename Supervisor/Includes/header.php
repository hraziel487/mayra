<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>PC Master Cancun</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- BOOTSTRAP 4 -->
    <link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
    <!-- FONT AWESOEM -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  </head>
  <body>
  
  <nav style="background:white" class="navbar navbar-expand-md  navbar-dark">
  <a class="navbar-brand" href="index.php">Panel administrador</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto ">
      <li class="nav-item">
        <a class="nav-link" href="clientes.php">Clientes</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="index.php">Productos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="closeSesion.php">Cerrar sesion</a>
      </li>    
    </ul>
  </div>  
</nav>
  </body>
  </html>
<?php include 'conexion.php';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>PC Master Cancun</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- BOOTSTRAP 4 -->
    <link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
    <!-- FONT AWESOEM -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  </head>
  <body>
  
  <nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Panel administrador</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="clientes.php">Clientes</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="inicio.php">Productos</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="ventas.php">Supervisores</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="cerrar.php">Cerrar sesion</a>
      </li>    
    </ul>
  </div>  
</nav>
<main class="container p-4">
  <div class="row">
    <div class="col-md-4">
    <!-----Validacion de mensajes dentro de sesion mensaje---->
      <!-- MENSAJES -->

      <?php if (isset($_SESSION['mensaje'])) { ?>
      <div class="alert alert-<?= $_SESSION['mensaje_color']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['mensaje']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <!-- //Limpiar los datos de sesion-->
      <?php session_unset(); } ?>

      <!--TABLA PRODUCTOS FORM -->
      <div class="card card-body">
        <form action="insercion-supervisor.php" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <input type="text" name="usuario" class="form-control" placeholder="Ingresar Nombre de Usuario" autofocus>
          </div>
                    <div class="form-group">
            <input type="text" name="clave" class="form-control" placeholder="Ingresar Clave del usuario" autofocus>
          </div>
                   <input type="submit" name="save_task" class="btn btn-success btn-block" value="Guardar Datos">
        </form>
      </div>
    </div>
    <div class="col-md-8">
    <!--//Tabla de acciones-->
      <table class="table table-bordered">
        <thead>
          <tr>
           <th>ID</th>
            <th>Usuario</th>
            <th>Clave</th>
            <th>Fecha</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
<!--Relleno de tabla-->
<!---Mostrar los datos que se consultaron------------>
          <?php
          $query = "SELECT * FROM supervisor";
          $result_tasks = mysqli_query($conexion, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['usuario']; ?></td>
            <td><?php echo $row['clave']; ?></td>
            <td><?php echo $row['fecha']; ?></td>
            <td>
              <a href="modificar-supervisor.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="eliminar-supervisor.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
